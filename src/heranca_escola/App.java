package heranca_escola;

/**
 * @author mastertech
 *
 *B) Crie um sistema que faça o registro das atividades de uma escola de ensino fundamental.
 *	- Uma turma é identificada por um ano letivo e por uma letra. Ex: 2A
 *	- Uma turma pode conter diversos alunos.
 *	- Um aluno deve ser registrado com nome, email, telefone e endereço.
 *	- Diversas matérias são lecionadas para uma turma. Uma matéria deve possuir um nome e um professor
 * responsável.
 *	- Um professor deve ser registrado com nome, email, cpf, telefone e endereço.
 *	- O sistema poderá ser acessado por alunos, professores e administradores. Todos os usuários devem 
 *ter email e senha para que seja possível o acesso.
 *	- Um administrador possui os mesmos campos do professor, com a diferença que ele não pode lecionar.
 */
public class App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
