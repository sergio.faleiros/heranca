package herenca_veiculo;

import java.util.ArrayList;

public class Caminhao extends Veiculo {
	
	ArrayList<Carreta> carreta = new ArrayList<Carreta>();
	
	public void setCarreta(int indice, Carreta carrreta) {
		
		carreta.add(indice, carrreta);
	}
	
	public void delCarreta(int indice) {
		
		carreta.remove(indice);
	}
}