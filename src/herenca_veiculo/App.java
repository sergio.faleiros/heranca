package herenca_veiculo;

import java.util.ArrayList;
import java.util.Scanner;
import java.nio.file.Paths;
import java.nio.file.Files;
import java.nio.file.Path;
import java.io.IOException;

/**
 * @author mastertech
 *A) Crie um sistema para registro de veículos.
 * - Um carro deve ser registrado com ano, modelo, placa, fabricante, cor e id.
 * - Um caminhão é um composto de veículos: o caminhão em si e um ou mais carretas.
 * - O caminhão tem os mesmos campos de um carro.
 * - Cada carreta de um caminhão deve possuir ano, modelo, placa, fabricante, comprimento, tipo e id. 
 *   Os tipos possíveis são: grãos, líquidos e baú.
 */
public class App {

	public static void main(String[] args) {
		
		boolean fim = false;
		int id = 0;
		ArrayList<Veiculo> lista = new ArrayList<Veiculo>();
		Scanner scanner = new Scanner(System.in);
		
		System.out.println(" ## Cadastro de Veiculos ## ");
		

		while (fim == false) {
			
			String menu = "";
			System.out.println("Digite 1 para cadastrar veiculo e 2 para cadastrar Caminhao 3 para sair");
			menu = scanner.nextLine();
			
			if (menu == "1") {
				Veiculo carro = new Veiculo();
				infoVeiculos(carro);
				id ++;
				carro.id = id;
//				lista.add(veiculo);
			}else {
				if (menu=="2") {
					Caminhao caminhao = new Caminhao();
					infoVeiculos(caminhao);
//					lista.add(caminhao);
				}else {
					fim = true;
					Path path = Paths.get("cadveiculos.txt");
					try {
						Files.write(path, lista.toString().getBytes());
					}catch (IOException e) {
						System.out.println("Erro ao gravar arquivo");
					}
				}
			}
		}
	}
	
	public static void infoVeiculos (Veiculo veiculo){
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Qual ano?");
		veiculo.ano = Integer.parseInt(scanner.nextLine());
		System.out.println("Qual cor?");
		veiculo.cor = scanner.nextLine();
		System.out.println("Qual fabricante?");
		veiculo.fabricante = scanner.nextLine();
		System.out.println("Qual modelo?");
		veiculo.modelo = scanner.nextLine();
		System.out.println("Qual placa?");
		veiculo.placa = scanner.nextLine();
	}
}
